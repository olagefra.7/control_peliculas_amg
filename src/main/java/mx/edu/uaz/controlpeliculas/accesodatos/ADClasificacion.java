package mx.edu.uaz.controlpeliculas.accesodatos;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.vaadin.ui.Notification;

import mx.edu.uaz.controlpeliculas.modelos.Clasificacion;
import mx.edu.uaz.controlpeliculas.modelos.Idioma;


public class ADClasificacion {
	
	public ADClasificacion(){
		 
	}
	
	public List<Clasificacion> obtenerTodasClasificaciones(){
		SqlSession sesion = Config.abreSesion();
		List<Clasificacion> clasificaciones = null;
		try {
			clasificaciones = sesion.selectList("obtenerTodasClasificaciones");
		} catch (Exception e) {
			Notification.show("No se puedieron recuperar lss clasificaciones de la BD "+e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}
		finally {
			sesion.close();
		}
		return clasificaciones;
	}
	
	
}
