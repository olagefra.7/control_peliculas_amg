package mx.edu.uaz.controlpeliculas.accesodatos;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.vaadin.ui.Notification;

import mx.edu.uaz.controlpeliculas.modelos.Idioma;


public class ADIdioma {
	
	public ADIdioma(){
		 
	}
	
	public List<Idioma> obtenerTodosIdiomas(){
		SqlSession sesion = Config.abreSesion();
		List<Idioma> idiomas = null;
		try {
			idiomas = sesion.selectList("obtenerTodosIdiomas");
		} catch (Exception e) {
			Notification.show("No se puedieron recuperar los idiomas de la BD "+e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}
		finally {
			sesion.close();
		}
		return idiomas;
	}
	
	
}
