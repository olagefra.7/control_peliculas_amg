package mx.edu.uaz.controlpeliculas.accesodatos;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.vaadin.ui.Notification;

import mx.edu.uaz.controlpeliculas.modelos.Director;
import mx.edu.uaz.controlpeliculas.modelos.Idioma;


public class ADDirector {
	
	public ADDirector(){
		 
	}
	
	public List<Director> obtenerTodosDirectores(){
		SqlSession sesion = Config.abreSesion();
		List<Director> directores = null;
		try {
			directores = sesion.selectList("obtenerTodosDirectores");
		} catch (Exception e) {
			Notification.show("No se puedieron recuperar los directores de la BD "+e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}
		finally {
			sesion.close();
		}
		return directores;
	}
	
	
}
