package mx.edu.uaz.controlpeliculas.accesodatos;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.vaadin.ui.Notification;

import mx.edu.uaz.controlpeliculas.modelos.Genero;
import mx.edu.uaz.controlpeliculas.modelos.Idioma;


public class ADGenero {
	
	public ADGenero(){
		 
	}
	
	public List<Genero> obtenerTodosGeneros(){
		SqlSession sesion = Config.abreSesion();
		List<Genero> generos = null;
		try {
			generos = sesion.selectList("obtenerTodosGeneros");
		} catch (Exception e) {
			Notification.show("No se puedieron recuperar los idiomas de la BD "+e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}
		finally {
			sesion.close();
		}
		return generos;
	}
	
	
}
