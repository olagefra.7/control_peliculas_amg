package mx.edu.uaz.controlpeliculas.accesodatos;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.vaadin.ui.Notification;

import mx.edu.uaz.controlpeliculas.modelos.Pelicula;
import mx.edu.uaz.controlpeliculas.utilerias.Hash;

public class ADPelicula {
	
	public ADPelicula() {
		
	}
	public boolean agregaPelicula(Pelicula pelicula){
		SqlSession sesion = Config.abreSesion();
		boolean ok = false;
		try {
			sesion.insert("agregaPelicula", pelicula);
			sesion.commit();
			ok = true;
			
		} catch (Exception e) {
			Notification.show("No se puede registra la película"+e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}
		finally {
			sesion.close();
		}
		return ok;
	}
	public List<Pelicula> obtenerTodasPeliculas(){
		SqlSession sesion = Config.abreSesion();
		List<Pelicula> peliculas = null;
		try {
			peliculas = sesion.selectList("obtenerTodasPeliculas");
			
		} catch (Exception e) {
			Notification.show("No se puedieron recuperar las peliculas de la BD "+e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}
		finally {
			sesion.close();
		}
		return peliculas;
	}

}
