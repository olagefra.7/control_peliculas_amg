package mx.edu.uaz.controlpeliculas.accesodatos;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.vaadin.ui.Notification;

import mx.edu.uaz.controlpeliculas.modelos.Usuario;
import mx.edu.uaz.controlpeliculas.utilerias.CadenaAleatoria;
import mx.edu.uaz.controlpeliculas.utilerias.EnviarCorreo;
import mx.edu.uaz.controlpeliculas.utilerias.Hash;

public class ADUsuario {
	
	public ADUsuario() {
		
	}
	public boolean agregaUsuario(Usuario usuario){
		SqlSession sesion = Config.abreSesion();
		boolean ok = false;
		try {
			usuario.setCadena(new CadenaAleatoria().getCadenaAleatoria(30));
			usuario.setPassword(Hash.sha1(usuario.getPassword()));
			sesion.insert("agregaUsuario", usuario);
			sesion.commit();
//			System.out.println(usuario.getIdUsuario());
			enviarCorreo(usuario);
			ok = true;
			
		} catch (Exception e) {
			Notification.show("No se puede registra el usuario"+e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}
		finally {
			sesion.close();
		}
		return ok;
	}
	
	public boolean editarUsuario(Usuario usuario, boolean passEquals){
		SqlSession sesion = Config.abreSesion();
		boolean ok = false;
		try {
			if (!passEquals)
				usuario.setPassword(Hash.sha1(usuario.getPassword()));
			sesion.update("editarUsuario", usuario);
			sesion.commit();
			ok = true;
			
		} catch (Exception e) {
			Notification.show("No se puede guardar el usuario"+e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}
		finally {
			sesion.close();
		}
		return ok;
	}
	
	public Usuario buscaUsuario(Usuario usuario){
		SqlSession sesion = Config.abreSesion();
		Usuario user = null;
		try {
			usuario.setPassword(Hash.sha1(usuario.getPassword()));
			user = sesion.selectOne("buscaUsuario", usuario);
		} catch (Exception e) {
			Notification.show("No se puedo buscar el usuario"+e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}
		finally {
			sesion.close();
		}
		return user;
	}
	
	
	public List<Usuario> obtenerTodosUsuarios(){
		SqlSession sesion = Config.abreSesion();
		List<Usuario> usuarios = null;
		try {
			usuarios = sesion.selectList("obtenerTodosUsuarios");
			
		} catch (Exception e) {
			Notification.show("No se puedieron recuperar los usuarios de la BD "+e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}
		finally {
			sesion.close();
		}
		return usuarios;
	}
	public boolean eliminarUsuarios(List<Usuario> usuarios) {
		boolean ok = false;
		SqlSession sesion = Config.abreSesion();
		try {
			sesion.delete("eliminarUsuarios", usuarios);
			sesion.commit();
			ok = true;
		} catch (Exception e) {
			Notification.show("No se puedieron eliminar los usuarios de la BD "+e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}
		finally {
			sesion.close();			
		}
		return ok;
		
	}
	public void enviarCorreo(Usuario usuario) {
		EnviarCorreo enviar = new EnviarCorreo();
		if (enviar.sendMail(usuario))
			Notification.show("Se envío correo para activación a "+usuario.getEmail());
	}
	public boolean confirmaCuenta(Usuario usuario) {
		boolean ok = false;
		SqlSession sesion = Config.abreSesion();
		try {
			Usuario user = sesion.selectOne("buscaUsuarioConfirm", usuario);
			System.out.println(user.getNombre());
			if (user != null) {
				System.out.println(user.getIdUsuario());
				sesion.update("confirmaCuenta", user);
				sesion.commit();
				ok = true;
			}
		}
		catch (Exception e) {
			Notification.show("No se pudo activar la cuenta"+e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}
		finally {
			sesion.close();			
		}
		return ok;
	}
	
}
