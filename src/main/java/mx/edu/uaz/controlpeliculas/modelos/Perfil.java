package mx.edu.uaz.controlpeliculas.modelos;

public class Perfil {
	private int idPerfil;
	private String perfil;
	
	public Perfil() {}
	
	public Perfil(int idPerfil, String perfil) {
		this.perfil = perfil;
		this.idPerfil = idPerfil;
	}

	public int getIdPerfil() {
		return idPerfil;
	}

	public void setIdPerfil(int idPerfil) {
		this.idPerfil = idPerfil;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	@Override
	public String toString() {
		return perfil;
	}
	
}
