package mx.edu.uaz.controlpeliculas.modelos;

public class Idioma {
	private int idIdioma;
	private String idioma;
	
	public Idioma() {}
	
	public Idioma(int idIdioma, String idioma) {
		this.idIdioma = idIdioma;
		this.idioma = idioma;
	}

	public int getIdIdioma() {
		return idIdioma;
	}

	public void setIdIdioma(int idIdioma) {
		this.idIdioma = idIdioma;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	
	@Override
	public String toString() {
		return idioma;
	}
	
}
