package mx.edu.uaz.controlpeliculas.modelos;

public class Clasificacion {
	private int idClasificacion;
	private String clasificacion, descripcion;
	
	public Clasificacion() {}

	public int getIdClasificacion() {
		return idClasificacion;
	}

	public void setIdClasificacion(int idClasificacion) {
		this.idClasificacion = idClasificacion;
	}

	public String getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public String toString() {
		return clasificacion;
	}
	
	
	
}
