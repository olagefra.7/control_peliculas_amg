package mx.edu.uaz.controlpeliculas.vistas;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import mx.edu.uaz.controlpeliculas.accesodatos.ADUsuario;
import mx.edu.uaz.controlpeliculas.modelos.Perfil;
import mx.edu.uaz.controlpeliculas.modelos.Usuario;

public class Login extends FormLayout {
	private TextField tfUsuario;
	private PasswordField pfContra;
	private Button btnIniciar, btnRegistrar;
	private HorizontalLayout hlBotones;
	
	public Login() {
		//this.setPrimaryStyleName("login");
		//this.setSizeFull();
		
		this.setResponsive(true);
		
		tfUsuario = new TextField();
		tfUsuario.setPlaceholder("Usuario");
		
		this.addComponent(tfUsuario);
		
		
		pfContra = new PasswordField();
		pfContra.setPlaceholder("Contraseña");
		this.addComponent(pfContra);
		
		btnIniciar = new Button("Iniciar Sesión");
		btnIniciar.setPrimaryStyleName("btn btn-primary");
		
		btnRegistrar = new Button("Registrar");
		btnRegistrar.setPrimaryStyleName("btn btn-success");
		
		hlBotones = new HorizontalLayout();
		hlBotones.addComponents(btnIniciar,btnRegistrar);
		
		this.addComponent(hlBotones);
		
		tfUsuario.setValue("admin");
		pfContra.setValue("Admin123@");
		
		btnIniciar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				ADUsuario adUsuario = new ADUsuario();
				Usuario usuario = new Usuario();
				
				usuario.setUsuario(tfUsuario.getValue());
				usuario.setPassword(pfContra.getValue());
				Usuario user = adUsuario.buscaUsuario(usuario);
				if (user != null) {
					if (user.getEstatus().equals("1")) {
						VaadinSession.getCurrent().getSession().setMaxInactiveInterval(350);
						VaadinSession.getCurrent().setAttribute("usuario", user);
						UI.getCurrent().setContent(new PrincipalLogicaDesign());
					}
					else
						Notification.show("Debes activar tu cuenta, revisa tu correo",Notification.Type.WARNING_MESSAGE);
				}
				else {
					Notification.show("Datos incorrectos",Notification.Type.ERROR_MESSAGE);
				}
			}
		});
		
		btnRegistrar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				UI.getCurrent().setContent(new FormUsuarioLogica());
			}
		});
	}
	
}
