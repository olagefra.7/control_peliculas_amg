package mx.edu.uaz.controlpeliculas.vistas;

import com.vaadin.data.Binder;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.Notification;
import com.vaadin.ui.NotificationConfiguration;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import mx.edu.uaz.controlpeliculas.accesodatos.ADUsuario;
import mx.edu.uaz.controlpeliculas.modelos.Usuario;
import mx.edu.uaz.controlpeliculas.utilerias.BuscarComponentes;

public class FormUsuarioLogica extends FormUsuario{
	private Binder<Usuario> binder;
	private Usuario usuario;
	private boolean edicion;
	private String contraAnt;
	
	public FormUsuarioLogica() {
		usuario = new Usuario();
		edicion = false;
		enlazar();
	}
	public FormUsuarioLogica(Usuario usuario) {
		this.usuario = usuario;
		contraAnt = usuario.getPassword();
		edicion = true;
		enlazar();
	}
	
	private void enlazar() {
		binder = new Binder<Usuario>();
		binder.setBean(usuario);
		
		binder.forField(tfUsuario)
			.asRequired("El usuario es requerido")
			.bind(Usuario::getUsuario, Usuario::setUsuario);
		
		String contraREGEX ="^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[{punct}])[A-Za-z\\d$@$!%*?&#.$($)$-$_]{8,32}[^'\\s]$";
		binder.forField(pfContra)
		.withValidator(new RegexpValidator("Contrasela inválida", contraREGEX))
		.bind(Usuario::getPassword, Usuario::setPassword);
		
		binder.forField(tfEmail)
		.withValidator(new EmailValidator("El formato de correo es incorrecto"))
		.bind(Usuario::getEmail, Usuario::setEmail);
		
		binder.forField(tfNombre)
		.asRequired("El nombre es requerido")
		.bind(Usuario::getNombre, Usuario::setNombre);
		
		binder.forField(tfApellidos)
		.asRequired("Los apellidos son requeridos")
		.bind(Usuario::getApellidos, Usuario::setApellidos);
		
		if (edicion) {
			btnRegistrar.setCaption("Guardar");
			btnLogin.setVisible(false);
		}
			
		
		btnRegistrar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				boolean passEquals = false;
				if (pfContra.getValue().equals(contraAnt)) {
					binder.removeBinding(pfContra);
					binder.forField(pfContra).bind(Usuario::getPassword, Usuario::setPassword);
					passEquals = true;
				}
				if (binder.validate().isOk()) {
					ADUsuario adUsuario = new ADUsuario();
					boolean ok = false;
					String mensaje = "Usuario registrado";
					if (edicion) {
						ok = adUsuario.editarUsuario(usuario, passEquals);
						mensaje = "Se guardó el usuario correctamente";
					}
						
					else
						ok = adUsuario.agregaUsuario(usuario);
					if (ok)
						Notification.show(mensaje);
					
					if (edicion)
					{
						usuario = new Usuario();
						binder.setBean(usuario);
						TabSheet tab = (TabSheet) BuscarComponentes.findComponentById(UI.getCurrent(), "tab-usuarios");
				    	tab.setSelectedTab(1);
//				    	tab.replaceComponent(tab.getTab(0).getComponent(), new FormUsuarioLogica(usuario));
					}
					
				}
				else
					Notification.show("Verifica los datos: ",Notification.Type.ERROR_MESSAGE);
			}
		});
		
		btnLogin.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				UI.getCurrent().setContent(new Login());
			}
		});
	}

}
