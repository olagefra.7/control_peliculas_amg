package mx.edu.uaz.controlpeliculas.vistas;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Image;
import com.vaadin.ui.Notification;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import mx.edu.uaz.controlpeliculas.accesodatos.ADClasificacion;
import mx.edu.uaz.controlpeliculas.accesodatos.ADDirector;
import mx.edu.uaz.controlpeliculas.accesodatos.ADGenero;
import mx.edu.uaz.controlpeliculas.accesodatos.ADIdioma;
import mx.edu.uaz.controlpeliculas.accesodatos.ADPelicula;
import mx.edu.uaz.controlpeliculas.modelos.Clasificacion;
import mx.edu.uaz.controlpeliculas.modelos.Director;
import mx.edu.uaz.controlpeliculas.modelos.Genero;
import mx.edu.uaz.controlpeliculas.modelos.Idioma;
import mx.edu.uaz.controlpeliculas.modelos.Pelicula;

public class FormPeliculas extends PeliculasFormDesign {
	private Binder<Pelicula> binder;
	private Pelicula pelicula;
	
	public FormPeliculas() {
		pelicula = new Pelicula();
		crearVista();
	}
	
	public FormPeliculas(Pelicula pelicula) {
		
		this.pelicula = pelicula;
		crearVista();
	}
	
	
	private void crearVista() {
		binder = new Binder<Pelicula>();
		binder.setBean(pelicula);
		
		binder.forField(tfTitulo)
			.withValidator(titulo -> titulo.length() >= 3,
				    "Debe contener al menos 3 caracteres")
			.bind(Pelicula::getTitulo, Pelicula::setTitulo);
		
		
		ADIdioma adIdioma = new ADIdioma();
		
		cboIdioma.setItems(adIdioma.obtenerTodosIdiomas());
		
		binder.forField(cboIdioma)
			.asRequired("El idioma es requerido")
			.bind(Pelicula::getIdioma, Pelicula::setIdioma);
		
		binder.forField(tfDuracion)
			.withConverter(new StringToIntegerConverter("Debe ser numérico"))
			.bind(Pelicula::getDuracion, Pelicula::setDuracion);
		
		binder.forField(rtaSinopsis)
			.asRequired("La sinopsis es requrida")
			.bind(Pelicula::getSinopsis, Pelicula::setSinopsis);
		
		ADGenero adGenero = new ADGenero();
		cboGenero.setItems(adGenero.obtenerTodosGeneros());
		binder.forField(cboGenero)
			.asRequired("El genero es requerido")
			.bind(Pelicula::getGenero, Pelicula::setGenero);
		
		ADDirector adDirector = new  ADDirector();
		cboDirector.setItems(adDirector.obtenerTodosDirectores());
		binder.forField(cboDirector)
			.asRequired("El director es requerido")
			.bind(Pelicula::getDirector, Pelicula::setDirector);
		
		ADClasificacion adClasifiacion = new ADClasificacion();
		cboClasificacion.setItems(adClasifiacion.obtenerTodasClasificaciones());
		binder.forField(cboClasificacion)
			.asRequired("La clasificación es requerida")
			.bind(Pelicula::getClasificacion, Pelicula::setClasificacion);
		
		btnAgrega.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (binder.validate().isOk()) {
					ADPelicula adPelicula = new ADPelicula();
					boolean ok = false;
					ok = adPelicula.agregaPelicula(pelicula);
					if (ok)
						Notification.show("Se agregó correctamente la película");
				}
				else
					Notification.show("Datos incorrectos",Notification.Type.ERROR_MESSAGE);
				
			}
		});
		
	}
	

}
