package mx.edu.uaz.controlpeliculas.vistas;

import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.MenuBar.MenuItem;

import mx.edu.uaz.controlpeliculas.modelos.Usuario;
import mx.edu.uaz.controlpeliculas.utilerias.ConfigVars;

import com.vaadin.ui.UI;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class PrincipalLogicaDesign extends PrincipalDesign {
	
	public PrincipalLogicaDesign() {
		
		imgUsuario.setSource(new ThemeResource("layouts/images/avatar/1.jpg"));
		Usuario usuario = (Usuario) VaadinSession.getCurrent().getAttribute("usuario");
//		Notification.show(usuario.getPerfil().toString());
		if (usuario.getPerfil().getIdPerfil() != 3)
			btnUsuarios.setVisible(false);
		
		MenuBar.Command command = new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				if (selectedItem.getText() == "Salir") {
					ConfirmDialog.show(
							UI.getCurrent(), 
							"Confirmar Salir del Sistema:", 
							"¿Deseas salir del sistema?",
						    "Salir", "Cancelar", 
						    new ConfirmDialog.Listener() {
						        public void onClose(ConfirmDialog dialog) {
					                if (dialog.isConfirmed()) {
					                		VaadinSession.getCurrent().close();
					                		UI.getCurrent().getPage().setLocation(ConfigVars.APP_NAME);
						              } 
				            }
				        });	
				}
				else {
					miContenido.removeAllComponents();
					miContenido.addComponent(new FormUsuarioLogica(usuario));
				}
			}
		};
		
		MenuItem userMenu = mnuUsuario.addItem(usuario.getNombre(), null);
		MenuItem datosMenu = userMenu.addItem("Cuenta", null, command);
		MenuItem salirMenu = userMenu.addItem("Salir", null, command);
		
		btnPeliculas.addClickListener(new Botones());
		btnUsuarios.addClickListener(new Botones());
	}
	
	class Botones implements ClickListener{

		@Override
		public void buttonClick(ClickEvent event) {
			miContenido.removeAllComponents();
			if (event.getButton() == btnUsuarios)
				miContenido.addComponent(new UsuariosTab());
			else if (event.getButton() == btnPeliculas)
				miContenido.addComponent(new PeliculasTab());
			
		}
		
		
	}
}
