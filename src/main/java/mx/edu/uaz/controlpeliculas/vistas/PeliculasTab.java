package mx.edu.uaz.controlpeliculas.vistas;

import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

public class PeliculasTab extends VerticalLayout{
	private TabSheet tab;
	
	public PeliculasTab(){
		tab = new TabSheet();
		tab.setId("tab-peliculas");
		tab.addTab(new FormPeliculas(), "Alta");
		tab.addTab(new PeliculasLista(), "Lista");
		
		this.addComponent(tab);
	}
}
