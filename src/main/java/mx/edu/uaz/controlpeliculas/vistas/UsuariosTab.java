package mx.edu.uaz.controlpeliculas.vistas;

import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

public class UsuariosTab extends VerticalLayout{
	private TabSheet tab;
	
	public UsuariosTab(){
		tab = new TabSheet();
		tab.setId("tab-usuarios");
		tab.addTab(new FormUsuarioLogica(), "Alta");
		tab.addTab(new UsuariosLista(), "Lista");
		
		this.addComponent(tab);
	}
}
