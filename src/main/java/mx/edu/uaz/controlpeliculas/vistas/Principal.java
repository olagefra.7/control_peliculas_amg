package mx.edu.uaz.controlpeliculas.vistas;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import mx.edu.uaz.controlpeliculas.modelos.Usuario;
import mx.edu.uaz.controlpeliculas.utilerias.ConfigVars;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class Principal extends CustomLayout{
	private Button btnRegistrarPelicula, btnLogout, btnAltaPelicula;
	private VerticalLayout vlPrincipal;
	private Usuario usuario;
	public Principal() {
		setTemplateName("principal");
		setPrimaryStyleName("principal-cl");
		setSizeFull();
		setResponsive(true);
		
		btnRegistrarPelicula = new Button("Registra película");
		btnRegistrarPelicula.setPrimaryStyleName("mi-boton");
		
		btnRegistrarPelicula.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				vlPrincipal.removeAllComponents();
				vlPrincipal.addComponent(new PeliculaForm());
				
			}
		});
		
		btnAltaPelicula = new Button("Alta película");
		btnAltaPelicula.setPrimaryStyleName("mi-boton");
		
		btnAltaPelicula.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				vlPrincipal.removeAllComponents();
				vlPrincipal.addComponent(new FormPeliculas());
				
			}
		});
		
		usuario = (Usuario) VaadinSession.getCurrent().getAttribute("usuario");
		
		vlPrincipal = new VerticalLayout();
		vlPrincipal.addComponent(
				new Label("Bienvenido "+
						usuario.getNombre() + " " +
						usuario.getApellidos()));
		
		addComponent(vlPrincipal, "mi-contenedor");
		addComponent(btnAltaPelicula, "alta-pelicula2"); 
		addComponent(btnRegistrarPelicula, "alta-pelicula"); 
		
		btnLogout = new Button("Salir");
		btnLogout.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				VaadinSession.getCurrent().close();
				UI.getCurrent().getPage().setLocation(ConfigVars.APP_NAME);
			}
		});
		
		addComponent(btnLogout, "salir");
	}

}
