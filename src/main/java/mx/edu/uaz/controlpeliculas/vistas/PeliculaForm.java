package mx.edu.uaz.controlpeliculas.vistas;


import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ui.NotificationRole;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Notification;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import mx.edu.uaz.controlpeliculas.modelos.Clasificacion;
import mx.edu.uaz.controlpeliculas.modelos.Director;
import mx.edu.uaz.controlpeliculas.modelos.Genero;
import mx.edu.uaz.controlpeliculas.modelos.Idioma;
import mx.edu.uaz.controlpeliculas.modelos.Pelicula;

public class PeliculaForm extends CustomLayout{
	
	private TextField tfTitulo, tfDuracion,tfMail,tfCurp;
	private RichTextArea rtaSinopsis;
	private ComboBox<Genero> cboGenero;
	private ComboBox<Director> cboDirector;
	private ComboBox<Clasificacion> cboClasificacion;
	private ComboBox<Idioma> cboIdioma;
	private Pelicula pelicula;
	private Button btnAgregar;
	private Binder<Pelicula> binder;
	private Image imagen;
	
	public PeliculaForm() {
		pelicula = new Pelicula();
		crearVista();
	}
	
	public PeliculaForm(Pelicula pelicula) {
		this.pelicula = pelicula;
		crearVista();
	}
	
	public void crearVista() {
		setTemplateName("pelicula");
		setResponsive(true);
		setSizeFull();
		
		binder = new Binder<Pelicula>();
		binder.setBean(pelicula);
		
		tfTitulo = new TextField("Título");
		tfTitulo.setWidth("100%");
		binder.forField(tfTitulo)
			.withValidator(titulo -> titulo.length() >= 3,
				    "Debe contener al menos 3 caracteres")
			.bind(Pelicula::getTitulo, Pelicula::setTitulo);
		
		List<Idioma> idiomas = new ArrayList<Idioma>();
		idiomas.add(new Idioma(1,"Español"));
		idiomas.add(new Idioma(2,"Inglés"));
		idiomas.add(new Idioma(3,"Alemán"));
		idiomas.add(new Idioma(4,"Portugues"));
		
		cboIdioma = new ComboBox<Idioma>("Idioma");
		
		cboIdioma.setItems(idiomas);
		cboIdioma.setWidth("100%");
		binder.forField(cboIdioma)
			.asRequired("El idioma es requerido")
			.bind(Pelicula::getIdioma, Pelicula::setIdioma);
		
		tfDuracion = new TextField("Duración");
		tfDuracion.setWidth("100%");
		binder.forField(tfDuracion)
			.withConverter(new StringToIntegerConverter("Debe ser numérico"))
			.bind(Pelicula::getDuracion, Pelicula::setDuracion);
		
		rtaSinopsis = new RichTextArea("Sinópsis");
		rtaSinopsis.setWidth("100%");

		binder.forField(rtaSinopsis)
			.asRequired("La sinopsis es requrida")
			.bind(Pelicula::getSinopsis, Pelicula::setSinopsis);
		
		cboGenero = new ComboBox<Genero>("Género");
		cboGenero.setWidth("100%");
		binder.forField(cboGenero)
			.asRequired("El genero es requerido")
			.bind(Pelicula::getGenero, Pelicula::setGenero);
		
		cboDirector = new ComboBox<Director>("Director");
		cboDirector.setWidth("100%");
		binder.forField(cboDirector)
			.asRequired("El director es requerido")
			.bind(Pelicula::getDirector, Pelicula::setDirector);
		
		cboClasificacion = new ComboBox<Clasificacion>("Clasificación");
		cboClasificacion.setWidth("100%");
		binder.forField(cboClasificacion)
			.asRequired("La clasificación es requerida")
			.bind(Pelicula::getClasificacion, Pelicula::setClasificacion);
		
		imagen = new Image();
		
		
		btnAgregar = new Button("Agregar",VaadinIcons.ADD_DOCK);
		btnAgregar.setPrimaryStyleName("btn btn-primary");
		
		
		/*tfMail = new TextField();
		binder.forField(tfMail)
			.withValidator(new EmailValidator("El formato de correo es incorrecto"))
			.bind();
			
		String curp = "/^([A-Z][AEIOUX][A-Z]{2}\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\\d])(\\d)$/";
		tfCurp = new TextField();
		binder.forField(tfCurp)
			.withValidator(new RegexpValidator("CURP inválido", curp))
			.bind();
		
		*/
		
		addComponent(tfTitulo, "titulo");
		addComponent(cboIdioma, "idioma");
		addComponent(tfDuracion, "duracion");
		addComponent(rtaSinopsis, "sinopsis");
		addComponent(cboGenero, "genero");
		addComponent(cboDirector, "director");
		addComponent(cboClasificacion, "clasificacion");
		addComponent(imagen, "imagen");
		addComponent(btnAgregar, "agregar");
		
		btnAgregar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				Notification.show(cboIdioma.getValue().toString());
				if (binder.validate().isOk()) {
					Notification.show("Datos correctos");
				}
				else
					Notification.show("Datos incorrectos",Notification.Type.ERROR_MESSAGE);
				
			}
		});
		
	}

}
