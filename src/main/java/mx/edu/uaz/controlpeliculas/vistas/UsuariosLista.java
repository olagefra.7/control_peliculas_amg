package mx.edu.uaz.controlpeliculas.vistas;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FontIcon;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.ImageRenderer;

import mx.edu.uaz.controlpeliculas.accesodatos.ADUsuario;
import mx.edu.uaz.controlpeliculas.modelos.Usuario;
import mx.edu.uaz.controlpeliculas.utilerias.BuscarComponentes;

public class UsuariosLista extends VerticalLayout{
	private Grid<Usuario> grid;
	private Button btnEliminar;
	
	public UsuariosLista() {
		grid = new Grid<Usuario>(Usuario.class);
		ADUsuario adUsuario = new ADUsuario();
		grid.setItems(adUsuario.obtenerTodosUsuarios());
		
		
		
		grid.setColumns("usuario","nombre","apellidos","email","perfil");
		
		Column<Usuario, ThemeResource> imageUser = grid.addColumn(
			    usuario -> new ThemeResource("layouts/images/avatar/1.jpg"),
			    new ImageRenderer()).setCaption("Avatar");
		
		grid.getColumn("email").setCaption("E-mail");
		grid.setWidth("100%");
		
		grid.setSelectionMode(SelectionMode.MULTI);
		
		grid.addComponentColumn(usuario -> {
		      Button button = new Button("",VaadinIcons.EDIT);
		      button.setStyleName("btn btn-danger");
		      button.addClickListener(click -> {
		    	  TabSheet tab = (TabSheet) BuscarComponentes.findComponentById(UI.getCurrent(), "tab-usuarios");
		    	  tab.setSelectedTab(0);
		    	  tab.replaceComponent(tab.getTab(0).getComponent(), new FormUsuarioLogica(usuario));
		      });
		      
		      return button;
		});
				
		
		btnEliminar = new Button("Eliminar");
		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if	(!grid.getSelectedItems().isEmpty()) {
					ConfirmDialog.show(
							UI.getCurrent(), 
							"Confirmar eliminación:", 
							"¿Deseas relamente eliminar los registros?",
						    "Eliminar", "Cancelar", 
						    new ConfirmDialog.Listener() {
						        public void onClose(ConfirmDialog dialog) {
					                if (dialog.isConfirmed()) {
					                	Set<Usuario> usuarios = grid.getSelectedItems();
										List<Usuario> users = new ArrayList<Usuario>();
										users.addAll(usuarios);
										ADUsuario adUsuario = new ADUsuario();
										boolean ok = adUsuario.eliminarUsuarios(users);
										if (ok){
											grid.setItems(adUsuario.obtenerTodosUsuarios());
											Notification.show("Registros eliminados...",Notification.Type.WARNING_MESSAGE);
										}
						              } 
						            }
						        });
					}
					else
						Notification.show("Selecciona al menos un usuario para eliminar",Notification.Type.WARNING_MESSAGE);
				}
		});
		
		
		this.addComponents(grid,btnEliminar);
		
	}
}
