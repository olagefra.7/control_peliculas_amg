package mx.edu.uaz.controlpeliculas;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.StyleSheet;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Viewport;
import com.vaadin.server.CustomizedSystemMessages;
import com.vaadin.server.SystemMessages;
import com.vaadin.server.SystemMessagesInfo;
import com.vaadin.server.SystemMessagesProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

import mx.edu.uaz.controlpeliculas.accesodatos.ADPelicula;
import mx.edu.uaz.controlpeliculas.modelos.Pelicula;
import mx.edu.uaz.controlpeliculas.modelos.Usuario;
import mx.edu.uaz.controlpeliculas.utilerias.ConfigVars;
import mx.edu.uaz.controlpeliculas.vistas.ConfirmarCuenta;
import mx.edu.uaz.controlpeliculas.vistas.Login;
import mx.edu.uaz.controlpeliculas.vistas.Principal;
import mx.edu.uaz.controlpeliculas.vistas.PrincipalDesign;
import mx.edu.uaz.controlpeliculas.vistas.PrincipalLogicaDesign;


@Theme("mytheme")
@Title("Administración de películas")
@Viewport("width=device-width, initial-scale=1")
@StyleSheet({
	"vaadin://css/bootstrap.min.css",
    "vaadin://css/font-awesome.min.css",
    "vaadin://css/themify-icons.css",
    "vaadin://css/flag-icon.min.css",
    "vaadin://css/cs-skin-elastic.css",
    "vaadin://css/jqvmap.min.css",
    "vaadin://css/style.css",
    "vaadin://css/estilos.css",
    "vaadin://css/bootstrap.min.css.map",
    "https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800"
})
@JavaScript({
	"vaadin://js/jquery.min.js",
    "vaadin://js/popper.min.js",
    "vaadin://js/bootstrap.min.js",
    "vaadin://js/main.js",
    "vaadin://js/dashboard.js",
    "vaadin://js/funciones.js",
})
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
    	String cadena = null;
		String usuarioS = null;
		try {
			cadena = vaadinRequest.getParameter("user");
			usuarioS = vaadinRequest.getParameter("logs");
		} catch (Exception e) {
			Notification.show("Parámetros no válidos");
		}
		if (cadena != null && usuarioS != null) {
			setContent(new ConfirmarCuenta(usuarioS, cadena));
		}
		else {
    	
			Usuario usuario = (Usuario) VaadinSession.getCurrent().getAttribute("usuario"); 
	        if (usuario == null)
	        	setContent(new Login());
	        else
	        	setContent(new PrincipalLogicaDesign());
		}
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    	@Override
	    protected void servletInitialized() throws ServletException {
	    	    	super.servletInitialized();
	    	    	getService().setSystemMessagesProvider(new SystemMessagesProvider() {
						
				@Override
				public SystemMessages getSystemMessages(SystemMessagesInfo systemMessagesInfo) {
					CustomizedSystemMessages mensajes = new CustomizedSystemMessages();
					mensajes.setSessionExpiredCaption("Expiró la sesión");
					mensajes.setSessionExpiredMessage("El tiempo de sesión se terminó, "
							+ "<br> puedes actualizar la página o hacer click <i><b>aqui</b></i>");
					mensajes.setSessionExpiredURL("/"+ConfigVars.APP_NAME);
					mensajes.setSessionExpiredNotificationEnabled(true);
					
					mensajes.setCommunicationErrorCaption("Error de comunicación con servidor");
					mensajes.setCommunicationErrorMessage("No existe comunicación con el servidor"
							+ "<br> Puedes recargar la página haciendo click <i><b>Aquí</b></i>");
					mensajes.setCommunicationErrorURL("/"+ConfigVars.APP_NAME);
					mensajes.setCommunicationErrorNotificationEnabled(true);
					
					mensajes.setInternalErrorCaption("Ocurrió un error interno");
					mensajes.setInternalErrorMessage("La aplicación experimentó un error interno"
							+ "<br> Puedes recargar la página haciendo click <i><b>Aquí</b></i>");
					mensajes.setInternalErrorURL("/"+ConfigVars.APP_NAME);
					mensajes.setInternalErrorNotificationEnabled(true);
					return mensajes;
				}
			});
	    }
    }
}
